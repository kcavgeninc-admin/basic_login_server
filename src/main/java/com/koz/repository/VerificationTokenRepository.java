package com.koz.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.koz.domain.VerificationToken;

public interface VerificationTokenRepository extends MongoRepository<VerificationToken, Long> {

	VerificationToken findByToken(String token);
	
}

package com.koz.repository;

import org.springframework.data.repository.CrudRepository;

import com.koz.domain.Role;

/**
 *
 * @author didin
 */
public interface RoleRepository extends CrudRepository<Role, String> {
    
    Role findByRole(String role);
}

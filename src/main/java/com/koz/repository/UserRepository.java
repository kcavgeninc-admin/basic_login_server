package com.koz.repository;


import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;

import com.koz.domain.User;


public interface UserRepository extends CrudRepository<User, String> {
    
    User findByEmail(String email);
    
    void deleteById(BigInteger id);
    
}

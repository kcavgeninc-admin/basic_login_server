package com.koz.registration;

import org.springframework.context.ApplicationEvent;

import com.koz.domain.User;

public class OnRegistrationCompleteEvent  extends ApplicationEvent {

	public final String appUrl;
	public final User user;
	
	public OnRegistrationCompleteEvent(final User user, final String appUrl) {
        super(user);
        this.user = user;
        this.appUrl = appUrl;
    }

    //

    public String getAppUrl() {
        return appUrl;
    }

    public User getUser() {
        return user;
    }

}
